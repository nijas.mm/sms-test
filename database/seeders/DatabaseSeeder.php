<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('teachers')->truncate();
        DB::table('teachers')->insert([
            'name' => 'Katie',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('teachers')->insert([
            'name' => 'Max',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('students')->truncate();
        DB::table('students')->insert([
            'name' => 'John Doe',
            'age' => 18,
            'gender' => 'Male',
            'teacher_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('students')->insert([
            'name' => 'Mary',
            'age' => 22,
            'gender' => 'Female',
            'teacher_id' => 2,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('subjects')->truncate();
        DB::table('subjects')->insert([
            'name' => 'MATHS',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('subjects')->insert([
            'name' => 'SCIENCE',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('subjects')->insert([
            'name' => 'HISTORY',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('terms')->truncate();
        DB::table('terms')->insert([
            'name' => 'ONE',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('terms')->insert([
            'name' => 'TWO',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
