<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Mark;
use App\Models\MarkItem;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Term;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class MarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Mark::select('marks.id', 'student_id', 'term_id', 'students.name', 'terms.name as term')
                ->leftjoin('students', 'students.id', 'marks.student_id')
                ->leftjoin('terms', 'terms.id', 'marks.term_id');
            $table =  DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $button = '<button type="button" data-href="' . route('mark.edit', $row->id) . '" class="btn btn-xs btn-primary  edit_button" data-container=".modal_class"><i class="fa fa-edit"></i></button>&nbsp';
                    $button .= '<button data-href="' . route('delete', ['mark', $row->id]) . '" class="btn btn-xs btn-danger btn-modal"><i class="fa fa-trash"></i></button>';
                    return $button;
                });
            $subjects = Subject::get();
            $mark = 0;
            foreach ($subjects as $subject) {
                $table = $table->addColumn($subject->name, function ($row) use ($subject, $mark) {
                    if (MarkItem::where('subject_id', $subject->id)->where('mark_id', $row->id)->exists()) {
                        $mark = $mark + MarkItem::where('subject_id', $subject->id)->where('mark_id', $row->id)->first()['marks'];
                        return MarkItem::where('subject_id', $subject->id)->where('mark_id', $row->id)->first()['marks'];
                    } else {
                        return '-';
                    }
                    //return MarkItem::where('subject_id', $subject->id)->where('mark_id', $row->id)->first() ? MarkItem::where('subject_id', $subject->id)->where('mark_id', $row->id)->first()['marks'] : '-';
                });
            }
            $table = $table->addColumn('date', function ($row) {
                return date('M d, Y h:i A', strtotime($row->created_at));
            });
            $table = $table->addColumn('total', function ($row) {
                return MarkItem::where('mark_id', $row->id)->sum('marks');
            })

                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->where(function ($w) use ($request) {
                            $search = $request->get('search');
                            $w->Where('students.name', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
            return $table;
        }
        $subjects = Subject::get();
        return view('admin.marks.index')->with(compact('subjects'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::get();
        $terms = Term::get();
        $subjects = Subject::get();
        return view('admin.marks.create')->with(compact('students', 'terms', 'subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $rules = [
                'student_id'     => 'required',
                'term_id'     => 'required',
                'subject_id'     => 'required|array',
                'mark'     => 'required|array',
            ];
            $messages = [
                'student_id.required' => 'Student Required',
            ];
            $validator = Validator::make(request()->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            if (Mark::where('student_id', $request->student_id)->where('term_id', $request->term_id)->exists()) {
                return response()->json(['status' => false, 'message' => 'Data Already exist, Try Update!']);
            }
            try {
                DB::beginTransaction();
                $mark = new Mark();
                $mark->student_id      = $request->student_id;
                $mark->term_id      = $request->term_id;
                $mark->save();

                foreach ($request->subject_id as $key => $value) {
                    $mark_item = new MarkItem();
                    $mark_item->mark_id = $mark->id;
                    $mark_item->subject_id = $value;
                    $mark_item->marks = $request->mark[$key];
                    $mark_item->save();
                }
                DB::commit();
                return response()->json(['status' => true, 'message' => 'Successfully created']);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['status' => false, 'message' => $e->getMessage()]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students = Student::get();
        $terms = Term::get();
        $subjects = Subject::get();
        $mark = Mark::where('id', $id)->first();
        foreach ($subjects as $v) {
            if (MarkItem::where('subject_id', $v->id)->where('mark_id', $id)->doesntExist()) {
                $item = new MarkItem();
                $item->mark_id = $id;
                $item->subject_id = $v->id;
                $item->marks = null;
                $item->save();
            }
        }
        $mark_items = MarkItem::where('mark_id', $id)->get();
        return view('admin.marks.edit')->with(compact('students', 'terms', 'subjects', 'mark', 'mark_items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (request()->ajax()) {
            $rules = [
                'student_id'     => 'required',
                'term_id'     => 'required',
                'mark_item'     => 'required|array',
                'mark'     => 'required|array',
            ];
            $messages = [
                'student_id.required' => 'Student Required',
            ];
            $validator = Validator::make(request()->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            try {
                if (Mark::where('id', '!=', $id)->where('student_id', $request->student_id)->where('term_id', $request->term_id)->exists()) {
                    return response()->json(['status' => false, 'message' => 'Data Already exist, Try Update!']);
                }
                DB::beginTransaction();
                $mark = Mark::findOrFail($id);
                $mark->student_id      = $request->student_id;
                $mark->term_id      = $request->term_id;
                $mark->save();

                foreach ($request->mark_item as $key => $value) {
                    $item = MarkItem::findOrFail($value);
                    $item->marks = $request->mark[$key];
                    $item->save();
                }
                DB::commit();
                return response()->json(['status' => true, 'message' => 'Successfully created']);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['status' => false, 'message' => $e->getMessage()]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (request()->ajax()) {
            try {
                DB::beginTransaction();
                $term = Mark::findOrFail($id);
                $term->delete();
                MarkItem::where('mark_id', $id)->delete();
                DB::commit();
                return response()->json(['status' => true, 'message' => 'Successfully deleted']);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['status' => false, 'message' => 'Something went Wrong!']);
            }
        }
    }
}
