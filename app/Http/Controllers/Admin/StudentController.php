<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Student::select('students.id', 'students.name', 'age', 'gender', 'teachers.name as teacher')->leftjoin('teachers', 'teachers.id', 'students.teacher_id')->orderBy('id', 'desc');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $button = '<button type="button" data-href="' . route('student.edit', $row->id) . '" class="btn btn-xs btn-primary edit_button" data-container=".modal_class"><i class="fa fa-edit"></i></button>&nbsp';
                    $button .= '<button data-href="' . route('delete', ['student', $row->id]) . '" class="btn btn-xs btn-danger btn-modal"><i class="fa fa-trash"></i></button>';
                    return $button;
                })
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->where(function ($w) use ($request) {
                            $search = $request->get('search');
                            $w->Where('name', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.students.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teachers = Teacher::get();
        return view('admin.students.create')->with(compact('teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $rules = [
                'name'     => 'required|unique:students',
                'age'     => 'required|numeric',
                'gender'     => 'required',
                'teacher_id'     => 'required',
            ];
            $messages = [
                'name.required' => 'Name Required',
            ];
            $validator = Validator::make(request()->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            try {
                $student = new Student();
                $student->name      = $request->name;
                $student->age      = $request->age;
                $student->gender      = $request->gender;
                $student->teacher_id      = $request->teacher_id;
                $student->save();
                return response()->json(['status' => true, 'message' => 'Successfully created']);
            } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => "Something went Wrong!!"]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = student::where('id', $id)->first();
        $teachers = Teacher::get();
        return view('admin.students.edit')->with(compact('student', 'teachers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (request()->ajax()) {
            $rules = [
                'name'     => 'required|unique:students,name,' . $id,
                'age'     => 'required',
                'gender'     => 'required',
                'teacher_id'     => 'required',
            ];
            $messages = [
                'name.required' => 'Name Required',
            ];
            $validator = Validator::make(request()->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            try {
                $student = student::findOrFail($id);
                $student->name      = $request->name;
                $student->age      = $request->age;
                $student->gender      = $request->gender;
                $student->teacher_id      = $request->teacher_id;
                $student->save();
                return response()->json(['status' => true, 'message' => 'Successfully updated']);
            } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => 'Something went Wrong!']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (request()->ajax()) {
            try {
                $student = Student::findOrFail($id);
                $student->delete();
                return response()->json(['status' => true, 'message' => 'Successfully deleted']);
            } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => 'Something went Wrong!']);
            }
        }
    }
}
