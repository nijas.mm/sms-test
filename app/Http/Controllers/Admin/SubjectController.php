<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MarkItem;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Subject::select('id', 'name')->orderBy('id', 'desc');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $button = '<button type="button" data-href="' . route('subject.edit', $row->id) . '" class="btn btn-xs btn-primary edit_button" data-container=".modal_class"><i class="fa fa-edit"></i></button>&nbsp';
                    $button .= '<button data-href="' . route('delete', ['subject', $row->id]) . '" class="btn btn-xs btn-danger btn-modal"><i class="fa fa-trash"></i></button>';
                    return $button;
                })
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->where(function ($w) use ($request) {
                            $search = $request->get('search');
                            $w->Where('name', 'LIKE', "%$search%");
                        });
                    }
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.subjects.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subjects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $rules = [
                'name'     => 'required|unique:subjects',
            ];
            $messages = [
                'name.required' => 'Name Required',
            ];
            $validator = Validator::make(request()->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            try {
                $subject = new Subject();
                $subject->name      = $request->name;
                $subject->save();
                return response()->json(['status' => true, 'message' => 'Successfully created']);
            } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => "Something went Wrong!!"]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Subject::where('id', $id)->first();
        return view('admin.subjects.edit')->with(compact('subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (request()->ajax()) {
            $rules = [
                'name'     => 'required|unique:subjects,name,' . $id,
            ];
            $messages = [
                'name.required' => 'Name Required',
            ];
            $validator = Validator::make(request()->all(), $rules, $messages);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            try {
                $subject = Subject::findOrFail($id);
                $subject->name      = $request->name;
                $subject->save();
                return response()->json(['status' => true, 'message' => 'Successfully updated']);
            } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => 'Something went Wrong!']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (request()->ajax()) {
            try {
                if (MarkItem::where('subject_id', $id)->exists()) {
                    return response()->json(['status' => false, 'message' => 'You Cannot delete This!']);
                }
                $subject = Subject::findOrFail($id);
                $subject->delete();
                return response()->json(['status' => true, 'message' => 'Successfully deleted']);
            } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => 'Something went Wrong!']);
            }
        }
    }
}
