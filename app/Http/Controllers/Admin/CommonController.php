<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class CommonController extends Controller
{
    public function delete($route, $id)
    {
        if (request()->ajax()) {
            return view('admin.layouts.delete')->with(compact('route', 'id'));
        }
    }
}
