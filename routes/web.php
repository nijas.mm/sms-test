<?php

use App\Http\Controllers\admin\CommonController;
use App\Http\Controllers\Admin\MarkController;
use App\Http\Controllers\Admin\StudentController;
use App\Http\Controllers\Admin\SubjectController;
use App\Http\Controllers\Admin\TeacherController;
use App\Http\Controllers\Admin\TermController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('admin')->group(function () {
    Route::group(['middleware' => 'auth:web'], function () {
        Route::resource('teacher', TeacherController::class);
        Route::get('delete/{route}/{id}', [CommonController::class, 'delete'])->name('delete');
        Route::resource('student', StudentController::class);
        Route::resource('subject', SubjectController::class);
        Route::resource('term', TermController::class);
        Route::resource('mark', MarkController::class);
    });
});
