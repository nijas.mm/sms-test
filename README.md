# SAMPLE PROJECT USING Admin LTE 3 with Laravel 8

This is a fresh Laravel 8 project where we work with [AdminLTE 3](https://adminlte.io/). You can clone it and start working on your Laravel 8 project with AdminLTE 3 already implemented.


## How to use

Code for [Laravel 8 + AdminLTE 3]

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__
- Edit database credentials in __.env__
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate__
- Run __php artisan db:seed__
- Run __php artisan serve__ (if you want to use other port add __--port=90__)


## License

Username : admin@admin.com and password : password

## More

- Feel free to send me an e-mail for support [mmnijas@gmail.com](mmnijas@gmail.com).
