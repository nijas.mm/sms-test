 <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="post" action="{{route('term.update',$term->id)}}" accept-charset="UTF-8" id="edit_form">

            <input name="_method" type="hidden" value="PUT">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">EDIT TERM</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name" class="control-label">NAME:*</label>
                    <input class="form-control" required="" value="{{$term->name}}" placeholder="NAME" autocomplete="off" name="name" type="text"  maxlength="15">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success save_button">UPDATE</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->