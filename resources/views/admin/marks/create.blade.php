<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form method="post" action="{{route('mark.store')}}" accept-charset="UTF-8" id="createForm">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">ADD MARKS</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name" class="control-label">MARKS:*</label>
                            <select class="form-control" required="" name="student_id">
                                <option value="">SELECT MARKS</option>
                                @foreach ($students as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name" class="control-label">TERM:*</label>
                            <select class="form-control" required="" name="term_id">
                                <option value="">SELECT TERM</option>
                                @foreach ($terms as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SUBJECT</th>
                                    <th>MARKS</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($subjects as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>
                                            <input type="hidden" name="subject_id[]" value="{{$item->id}}">
                                            <input type="text" name="mark[]" class="form-control" required placeholder="Mark">
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                
                



                
            <div class="modal-footer">
                <button type="submit" class="btn btn-success save_button">SAVE</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
            </div>
        </form>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
