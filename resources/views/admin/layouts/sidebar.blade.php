@include('admin.layouts.navbar')
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin')}}" class="brand-link text-center">
      <span class="brand-text font-weight-light">SMS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('img/user.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         
          <li class="nav-item"><a href="{{route('teacher.index')}}" class="nav-link"><i class="fas fa-chalkboard-teacher nav-icon"></i><p>TEACHERS</p></a></li>
          <li class="nav-item"><a href="{{route('student.index')}}" class="nav-link"><i class="fas fa-user-graduate nav-icon"></i><p>STUDENT</p></a></li>
          <li class="nav-item"><a href="{{route('subject.index')}}" class="nav-link"><i class="fas fa-book-open nav-icon"></i><p>SUBJECT</p></a></li>
          <li class="nav-item"><a href="{{route('term.index')}}" class="nav-link"><i class="fas fa-bell nav-icon"></i><p>TERMS</p></a></li>
          <li class="nav-item"><a href="{{route('mark.index')}}" class="nav-link"><i class="fas fa-award nav-icon"></i><p>MARKS</p></a></li>
          
          
          <li class="nav-item">
            <a href="javascript:void" class="nav-link" onclick="$('#logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>LOGOUT</p>
            </a>
          </li>
          
        </ul>
       
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>