 <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="post" action="{{route('student.update',$student->id)}}" accept-charset="UTF-8" id="edit_form">
            <input name="_method" type="hidden" value="PUT">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">EDIT STUDENT</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name" class="control-label">NAME:*</label>
                    <input class="form-control" value="{{$student->name}}" required="" placeholder="NAME" autocomplete="off" name="name" type="text"  maxlength="50">
                </div>
                <div class="form-group">
                    <label for="name" class="control-label">AGE:*</label>
                    <input class="form-control" required="" value="{{$student->age}}" placeholder="AGE" autocomplete="off" name="age" type="text"  maxlength="10">
                </div>
                <div class="form-group">
                    <label for="name" class="control-label">GENDER:*</label>
                    <select class="form-control" required="" name="gender">
                        <option value="">SELECT</option>
                        <option value="Male" @if ($student->gender=='Male') selected @endif>Male</option>
                        <option value="Female" @if ($student->gender=='Female') selected @endif>Female</option>
                        <option value="Other" @if ($student->gender=='Other') selected @endif>Other</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name" class="control-label">REPORTING TEACHER:*</label>
                    <select class="form-control" required="" name="teacher_id">
                        <option value="">SELECT</option>
                        @foreach ($teachers as $item)
                            <option value="{{$item->id}}" @if ($student->teacher_id==$item->id) selected @endif>{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success save_button">UPDATE</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->