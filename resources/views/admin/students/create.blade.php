<div class="modal-dialog" role="document">
    <div class="modal-content">
        <form method="post" action="{{route('student.store')}}" accept-charset="UTF-8" id="createForm">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">ADD STUDENT</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name" class="control-label">NAME:*</label>
                    <input class="form-control" required="" placeholder="NAME" autocomplete="off" name="name" type="text"  maxlength="50">
                </div>
                <div class="form-group">
                    <label for="name" class="control-label">AGE:*</label>
                    <input class="form-control" required="" placeholder="AGE" autocomplete="off" name="age" type="text"  maxlength="10">
                </div>
                <div class="form-group">
                    <label for="name" class="control-label">GENDER:*</label>
                    <select class="form-control" required="" name="gender">
                        <option value="">SELECT</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name" class="control-label">REPORTING TEACHER:*</label>
                    <select class="form-control" required="" name="teacher_id">
                        <option value="">SELECT</option>
                        @foreach ($teachers as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success save_button">SAVE</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">CLOSE</button>
            </div>
        </form>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
